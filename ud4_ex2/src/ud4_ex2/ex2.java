package ud4_ex2;

public class ex2 {

	public static void main(String[] args) {
		
		/*2. Escribe un programa Java que realice lo siguiente: declarar una variable N de tipo int,
		una variable A de tipo double y una variable C de tipo char y asigna a cada una un valor*/
		
		int n = 12;
		double a = 15.5;
		char c = 'a';
		
		System.out.println("Variable N = "+n+"\nVariable A = "+a+"\nVariable C = "+c);
		System.out.println(n +" + "+ a +" = "+(a+n));
		System.out.println(a +" - "+ n +" = "+(a-n));
		System.out.println("Valor num�rico del car�cter a "+(int) c);
	}

}
